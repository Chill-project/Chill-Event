<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Initialize the bundle chill_event
 */
class Version20160318111334 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE chill_event_event_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_event_role_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_event_status_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_event_event_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE chill_event_participation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE chill_event_event_type ('
                . 'id INT NOT NULL, name JSON NOT NULL, '
                . 'active BOOLEAN NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_event_role ('
                . 'id INT NOT NULL, '
                . 'type_id INT DEFAULT NULL, '
                . 'name JSON NOT NULL, '
                . 'active BOOLEAN NOT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AA714E54C54C8C93 ON chill_event_role (type_id)');
        $this->addSql('CREATE TABLE chill_event_status (id INT NOT NULL, '
                . 'type_id INT DEFAULT NULL, '
                . 'name JSON NOT NULL, '
                . 'active BOOLEAN NOT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A6CC85D0C54C8C93 ON chill_event_status (type_id)');
        $this->addSql('CREATE TABLE chill_event_event ('
                . 'id INT NOT NULL, '
                . 'name VARCHAR(150) NOT NULL, '
                . 'date DATE NOT NULL, '
                . 'center_id INT DEFAULT NULL, '
                . 'type_id INT DEFAULT NULL, '
                . 'circle_id INT DEFAULT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE chill_event_participation ('
                . 'id INT NOT NULL, '
                . 'event_id INT DEFAULT NULL, '
                . 'person_id INT DEFAULT NULL, '
                . 'role_id INT DEFAULT NULL, '
                . 'status_id INT DEFAULT NULL, '
                . 'lastUpdate TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, '
                . 'PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_4E7768AC71F7E88B ON chill_event_participation (event_id)');
        $this->addSql('CREATE INDEX IDX_4E7768AC217BBB47 ON chill_event_participation (person_id)');
        $this->addSql('CREATE INDEX IDX_4E7768ACD60322AC ON chill_event_participation (role_id)');
        $this->addSql('CREATE INDEX IDX_4E7768AC6BF700BD ON chill_event_participation (status_id)');
        $this->addSql('CREATE INDEX IDX_FA320FC85932F377 ON chill_event_event (center_id)');
        $this->addSql('CREATE INDEX IDX_FA320FC8C54C8C93 ON chill_event_event (type_id)');
        $this->addSql('CREATE INDEX IDX_FA320FC870EE2FF6 ON chill_event_event (circle_id)');
        
        $this->addSql('ALTER TABLE chill_event_event '
              . 'ADD CONSTRAINT FK_FA320FC85932F377 FOREIGN KEY (center_id) '
              . 'REFERENCES centers (id) '
              . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_event '
              . 'ADD CONSTRAINT FK_FA320FC870EE2FF6 FOREIGN KEY (circle_id) '
              . 'REFERENCES scopes (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_event '
              . 'ADD CONSTRAINT FK_FA320FC8C54C8C93 FOREIGN KEY (type_id) '
              . 'REFERENCES chill_event_event_type (id) '
              . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_role '
                . 'ADD CONSTRAINT FK_AA714E54C54C8C93 FOREIGN KEY (type_id) '
                . 'REFERENCES chill_event_event_type (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_status '
                . 'ADD CONSTRAINT FK_A6CC85D0C54C8C93 '
                . 'FOREIGN KEY (type_id) '
                . 'REFERENCES chill_event_event_type (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_participation '
                . 'ADD CONSTRAINT FK_4E7768AC71F7E88B '
                . 'FOREIGN KEY (event_id) '
                . 'REFERENCES chill_event_event (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_participation '
                . 'ADD CONSTRAINT FK_4E7768AC217BBB47 '
                . 'FOREIGN KEY (person_id) '
                . 'REFERENCES Person (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_participation '
                . 'ADD CONSTRAINT FK_4E7768ACD60322AC '
                . 'FOREIGN KEY (role_id) '
                . 'REFERENCES chill_event_role (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE chill_event_participation '
                . 'ADD CONSTRAINT FK_4E7768AC6BF700BD '
                . 'FOREIGN KEY (status_id) '
                . 'REFERENCES chill_event_status (id) '
                . 'NOT DEFERRABLE INITIALLY IMMEDIATE');
        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE chill_event_role DROP CONSTRAINT FK_AA714E54C54C8C93');
        $this->addSql('ALTER TABLE chill_event_status DROP CONSTRAINT FK_A6CC85D0C54C8C93');
        $this->addSql('ALTER TABLE chill_event_participation DROP CONSTRAINT FK_4E7768ACD60322AC');
        $this->addSql('ALTER TABLE chill_event_participation DROP CONSTRAINT FK_4E7768AC6BF700BD');
        $this->addSql('ALTER TABLE chill_event_participation DROP CONSTRAINT FK_4E7768AC71F7E88B');
        // drop center_id constraint
        $this->addSql('ALTER TABLE chill_event_event DROP CONSTRAINT FK_FA320FC85932F377');
        // drop type_id constraint
        $this->addSql('ALTER TABLE chill_event_event DROP CONSTRAINT FK_FA320FC8C54C8C93');
        // drop circle_id constraint
        $this->addSql('ALTER TABLE chill_event_event DROP CONSTRAINT FK_FA320FC870EE2FF6');
        
        $this->addSql('DROP SEQUENCE chill_event_event_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_event_role_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_event_status_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_event_event_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE chill_event_participation_id_seq CASCADE');
        $this->addSql('DROP TABLE chill_event_event_type');
        $this->addSql('DROP TABLE chill_event_role');
        $this->addSql('DROP TABLE chill_event_status');
        $this->addSql('DROP TABLE chill_event_event');
        $this->addSql('DROP TABLE chill_event_participation');
        
    }
}
