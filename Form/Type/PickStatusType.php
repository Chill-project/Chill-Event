<?php

/*
 * Chill is a software for social workers
 * 
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\EventBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\EventBundle\Entity\Status;
use Chill\EventBundle\Entity\EventType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Allow to pick amongst type
 * 
 * parameters : 
 * 
 * - event_type : restricts to a certain event type. Default null (= all event types)
 * - active_only: restricts to active type only. Default true
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class PickStatusType extends AbstractType
{
   
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    /**
     *
     * @var TranslatorInterface
     */
    protected $translator;
    
    /**
     *
     * @var EntityRepository
     */
    protected $statusRepository;
    
    public function __construct(
          TranslatableStringHelper $translatableStringHelper,
          TranslatorInterface $translator,
          EntityRepository $statusRepository
    ) {
        $this->translatableStringHelper = $translatableStringHelper;
        $this->translator = $translator;
        $this->statusRepository = $statusRepository;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $qb = $options['query_builder'];
  
        if ($options['event_type'] instanceof EventType) {
            $options['query_builder']->where($qb->expr()->eq('r.type', ':event_type'))
                    ->setParameter('event_type', $options['event_type']);
            
        }

        if ($options['active_only'] === true) {
            $options['query_builder']->andWhere($qb->expr()->eq('r.active', ':active'))
                    ->setParameter('active', true);
        }
           
        if ($options['group_by'] === null && $options['event_type'] === null) {     
            $builder->addEventListener(
                  FormEvents::PRE_SET_DATA, 
                  function(FormEvent $event) {
                        $form = $event->getForm();
                        $name = $form->getName();
                        $config = $form->getConfig();
                        $type = $config->getType()->getName();
                        $options = $config->getOptions();
                        $form->getParent()->add($name, $type,  array_replace($options, array(
                           'group_by' => function(Status $s) 
                                { return $this->translatableStringHelper->localize($s->getType()->getName()); }
                        )));
                    }
            );
        }
        
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        // create copy for use in Closure
        $translatableStringHelper = $this->translatableStringHelper;
        $translator = $this->translator;
        
        $resolver
              // add option "event_type"
            ->setDefined('event_type')
            ->setAllowedTypes('event_type', array('null', EventType::class))
            ->setDefault('event_type', null)
            // add option allow unactive
            ->setDefault('active_only', true)
            ->setAllowedTypes('active_only', array('boolean'))
            ;
            
        $qb = $this->statusRepository->createQueryBuilder('r');
        
        $resolver->setDefaults(array(
           'class' => Status::class,
           'query_builder' => $qb,
           'group_by' => null,
           'choice_attr' => function(Status $s) {
                        return array(
                            'data-event-type' => $s->getType()->getId(),
                            'data-link-category' => $s->getType()->getId()
                        );
                    },
            'choice_label' => function(Status $s) 
                use ($translatableStringHelper, $translator) {
                    return $translatableStringHelper->localize($s->getName()).
                          ($s->getActive() === true ? '' :
                                ' ('.$translator->trans('unactive').')');
                    }
            ));
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
}
