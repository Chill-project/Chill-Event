<?php

/*
 * Chill is a software for social workers
 * 
 * Copyright (C) 2016, Champs Libres Cooperative SCRLFS, 
 * <http://www.champs-libres.coop>, <info@champs-libres.coop>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\EventBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Chill\EventBundle\Entity\EventType;

/**
 * Description of TranslatableEventType
 *
 * @author Champs-Libres Coop
 */
class PickEventType extends AbstractType
{
    /**
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $helper)
    {
        $this->translatableStringHelper = $helper;
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $helper = $this->translatableStringHelper;
        $resolver->setDefaults(
            array(
                'class' => EventType::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('et')
                    ->where('et.active = true');
                },
                'choice_label' => function (EventType $t) use ($helper) {
                    return $helper->localize($t->getName());
                },
                'choice_attrs' => function (EventType $t) {
                    return array('data-link-category' => $t->getId());
                }
            )
        );
    }
}
