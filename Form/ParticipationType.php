<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\EventBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\EventBundle\Form\Type\PickRoleType;
use Chill\EventBundle\Form\Type\PickStatusType;

/**
 * A type to create a participation
 * 
 * If the `event` option is defined, the role will be restricted
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ParticipationType extends AbstractType
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $translatableStringHelper)
    {
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // local copy of variable for Closure
        $translatableStringHelper = $this->translatableStringHelper;
        
        // add role
        $builder->add('role', PickRoleType::class, array(
           'event_type' => $options['event_type']
        ));
            
        // add a status
        $builder->add('status', PickStatusType::class, array(
           'event_type' => $options['event_type']
        ));
            
    }
    
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined('event_type')
                ->setAllowedTypes('event_type', array('null', EventType::class))
                ->setDefault('event_type', 'null');
    }
}
