<?php

namespace Chill\EventBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Chill\MainBundle\Form\Type\TranslatableStringFormType;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\EventBundle\Entity\EventType;

class RoleType extends AbstractType
{
    /**
     *
     * @var TranslatableStringHelper
     */
    protected $translatableStringHelper;
    
    public function __construct(TranslatableStringHelper $translatableStringHelper) {
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TranslatableStringFormType::class)
            ->add('active')
            ->add('type', EntityType::class, array(
                'class' => EventType::class,
                'choice_label' => function (EventType $e) { 
                    return $this->translatableStringHelper->localize($e->getName());
                }
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\EventBundle\Entity\Role'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chill_eventbundle_role';
    }
}
