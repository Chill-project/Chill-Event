<?php

namespace Chill\EventBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Chill\EventBundle\Form\Type\PickEventType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\MainBundle\Templating\TranslatableStringHelper;
use Chill\MainBundle\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Chill\MainBundle\Entity\Center;
use Chill\MainBundle\Entity\Scope;

use Symfony\Component\Security\Core\Role\Role;

class EventType extends AbstractType
{
        
    /** @var the user running this form */
    protected $user;

    /** @var AuthorizationHelper  */
    protected $authorizationHelper;

    /** @var TranslatableStringHelper */
    protected $translatableStringHelper;
    
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationHelper $authorizationHelper,
        TranslatableStringHelper $translatableStringHelper
    ) {
        if (!$tokenStorage->getToken()->getUser() instanceof User) {
            throw new \RuntimeException("you should have a valid user");
        }
        $this->user = $tokenStorage->getToken()->getUser();
        $this->authorizationHelper = $authorizationHelper;
        $this->translatableStringHelper = $translatableStringHelper;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userReachableCenters = $this->authorizationHelper
            ->getReachableCenters($this->user, new Role('CHILL_EVENT_CREATE'));
            
        $userReachableCirclesByCircleId = array();
        $userReachableCentersByCircleId = array();
        
        foreach ($userReachableCenters as $center) {
            foreach ($this->authorizationHelper
                    ->getReachableCircles($this->user, new Role('CHILL_EVENT_CREATE'), $center) as $circle) {
                if (array_key_exists($circle->getId(), $userReachableCirclesByCircleId)) {
                    array_push($userReachableCentersByCircleId[$circle->getId()], $center);
                } else {
                    $userReachableCirclesByCircleId[$circle->getId()] = $circle;
                    $userReachableCentersByCircleId[$circle->getId()] = array($center);
                }
            }
        }
                
        $builder
            ->add('name')
            ->add(
                'date',
                'date',
                array(
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd-MM-yyyy'
                )
            )
            ->add('center', EntityType::class, array(
                'class' => Center::class,
                'choices' => $userReachableCenters,
                'attr' => array('class' => 'select2 chill-category-link-parent'),
                'choice_attr' => function (Center $center) {
                    return array(
                        'class' => ' chill-category-link-parent',
                        'data-link-category' => $center->getId()
                    );
                },
            ))
            ->add('circle', EntityType::class, array(
                'class' => Scope::class,
                'choices' => array_values($userReachableCirclesByCircleId),
                'choice_label' => function ($circle) {
                    $helper = $this->translatableStringHelper;
                    return $helper->localize($circle->getName());
                },
                'choice_attr' => function ($circle) use ($userReachableCentersByCircleId) {
                    $centersId  = "";
                    foreach ($userReachableCentersByCircleId[$circle->getId()] as $center) {
                        $centersId = $centersId.($center->getId()).',';
                    }
                    $centersId = trim($centersId, ',');
                    return array(
                        'data-link-categories' => $centersId,

                    );
                },
            ))
            ->add('type', PickEventType::class)
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Chill\EventBundle\Entity\Event'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'chill_eventbundle_event';
    }
}
