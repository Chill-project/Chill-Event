<?php

namespace Chill\EventBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Chill\EventBundle\Security\Authorization\EventVoter;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ChillEventExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, 
              new FileLocator(__DIR__.'/../Resources/config/services'));
        $loader->load('repositories.yml');
        $loader->load('search.yml');
        $loader->load('authorization.yml');
        $loader->load('forms.yml');
    }
    
     /* (non-PHPdoc)
      * @see \Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface::prepend()
      */
    public function prepend(ContainerBuilder $container) 
    {
        $this->prependAuthorization($container);
        $this->prependRoute($container);
    }
    
    /**
     * add route to route loader for chill
     * 
     * @param ContainerBuilder $container
     */
    protected function prependRoute(ContainerBuilder $container)
    {
        //add routes for custom bundle
         $container->prependExtensionConfig('chill_main', array(
           'routing' => array(
              'resources' => array(
                 '@ChillEventBundle/Resources/config/routing.yml'
              )
           )
        ));
    }
    
    /**
     * add authorization hierarchy
     * 
     * @param ContainerBuilder $container
     */
    protected function prependAuthorization(ContainerBuilder $container)
    {
        $container->prependExtensionConfig('security', array(
           'role_hierarchy' => array(
                EventVoter::SEE_DETAILS => array(EventVoter::SEE),
                EventVoter::UPDATE => array(EventVoter::SEE_DETAILS),
                EventVoter::CREATE => array(EventVoter::SEE_DETAILS)
           )
        ));
    }
}
