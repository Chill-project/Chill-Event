<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\EventBundle\Security\Authorization;

use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\EventBundle\Entity\Participation;
use Chill\MainBundle\Entity\User;

/**
 * 
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class ParticipationVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    /**
     *
     * @var AuthorizationHelper
     */
    protected $authorizationHelper;
    
    const CREATE = 'CHILL_EVENT_PARTICIPATION_CREATE';
    const UPDATE = 'CHILL_EVENT_PARTICIPATION_UPDATE';
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->authorizationHelper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return array(
            self::CREATE, self::UPDATE
        );
    }

    protected function getSupportedClasses()
    {
        return array(
            Participation::class
        );
    }

    protected function isGranted($attribute, $participation, $user = null)
    {
        if (!$user instanceof User) {
            return false;
        } 
        
        return $this->authorizationHelper->userHasAccess($user, $participation, $attribute);
    }

    public function getRoles()
    {
        return $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return null;
    }
    
    public function getRolesWithHierarchy()
    {
        return [ 'Event' => $this->getRoles() ];
    }

}
