<?php



namespace Chill\EventBundle\Security\Authorization;

use Chill\MainBundle\Security\Authorization\AbstractChillVoter;
use Chill\MainBundle\Security\ProvideRoleHierarchyInterface;
use Chill\EventBundle\Entity\Event;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Chill\MainBundle\Entity\User;

/**
 * Description of EventVoter
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class EventVoter extends AbstractChillVoter implements ProvideRoleHierarchyInterface
{
    
    const SEE = 'CHILL_EVENT_SEE';
    const SEE_DETAILS = 'CHILL_EVENT_SEE_DETAILS';
    const CREATE = 'CHILL_EVENT_CREATE';
    const UPDATE = 'CHILL_EVENT_UPDATE';
    
    protected $authorizationHelper;
    
    public function __construct(AuthorizationHelper $helper)
    {
        $this->authorizationHelper = $helper;
    }
    
    protected function getSupportedAttributes()
    {
        return array(self::SEE, self::SEE_DETAILS, 
           self::CREATE, self::UPDATE);
    }

    protected function getSupportedClasses()
    {
        return array(Event::class);
    }

    protected function isGranted($attribute, $event, $user = null)
    { 
        if (!$user instanceof User) {
            return false;
        } 
        
        return $this->authorizationHelper->userHasAccess($user, $event, $attribute);
    }

    public function getRoles()
    {
        return $this->getSupportedAttributes();
    }

    public function getRolesWithoutScope()
    {
        return null;
    }
    
        
    public function getRolesWithHierarchy()
    {
        return [ 'Event' => $this->getRoles() ];
    }

}
