<?php


namespace Chill\EventBundle\Search;

use Chill\MainBundle\Search\AbstractSearch;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Chill\MainBundle\Security\Authorization\AuthorizationHelper;
use Symfony\Component\Templating\EngineInterface as TemplatingEngine;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\Role\Role;
use Chill\MainBundle\Pagination\PaginatorFactory;
use Chill\MainBundle\Search\SearchInterface;

/**
 * Search within Events.
 *
 * available terms :
 * - name: to search within the name
 * - date: search the event at a the given date
 * - date-from: search the event after the given date
 * - date-to : search the event before the given date
 *
 * Default terms search within the name, but the term "name" has precedence. This
 * means that if the search string is `@event xyz name:"abc"`, the searched
 * string will be "abc" and not xyz
 *
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class EventSearch extends AbstractSearch
{
    
    /**
     *
     * @var EntityRepository
     */
    private $er;
    
    /**
     *
     * @var \Chill\MainBundle\Entity\User
     */
    private $user;
    
    /**
     *
     * @var AuthorizationHelper
     */
    private $helper;
    
    /**
     *
     * @var TemplatingEngine
     */
    private $templating;
    
    /**
     *
     * @var PaginatorFactory
     */
    private $paginationFactory;
    
    const NAME = 'event_regular';
    
    public function __construct(
          TokenStorageInterface $tokenStorage,
          EntityRepository $eventRepository,
          AuthorizationHelper $authorizationHelper,
          TemplatingEngine $templating,
          PaginatorFactory $paginatorFactory
          )
    {
        $this->user = $tokenStorage->getToken()->getUser();
        $this->er = $eventRepository;
        $this->helper = $authorizationHelper;
        $this->templating = $templating;
        $this->paginationFactory = $paginatorFactory;
    }
    
    public function supports($domain)
    {
        return 'event' === $domain or 'events' === $domain;
    }
    
    public function isActiveByDefault()
    {
        return true;
    }
    
    public function getOrder()
    {
        return 3000;
    }

    public function renderResult(array $terms, $start = 0, $limit = 50,
          array $options = array())
    {
        $total = $this->count($terms);
        $paginator = $this->paginationFactory->create($total);
        
        return $this->templating->render('ChillEventBundle:Event:list.html.twig',
                array(
                    'events' => $this->search($terms, $start, $limit, $options),
                    'pattern' => $this->recomposePattern($terms, $this->getAvailableTerms(), $terms['_domain']),
                    'total' => $total,
                    'start' => $start,
                    'preview' => $options[SearchInterface::SEARCH_PREVIEW_OPTION],
                    'paginator' => $paginator,
                    'search_name' => self::NAME
                ));
    }
    
    protected function getAvailableTerms()
    {
        return array('date-from', 'date-to', 'name', 'date');
    }
    
    protected function search(array $terms, $start, $limit, $options)
    {
        $qb = $this->er->createQueryBuilder('e');
        $qb->select('e');
        $this->composeQuery($qb, $terms)
              ->setMaxResults($limit)
              ->setFirstResult($start)
              ->orderBy('e.date', 'DESC')
              ;
        
        return $qb->getQuery()->getResult();
    }
    
    protected function count(array $terms)
    {
        $qb = $this->er->createQueryBuilder('e');
        $qb->select('COUNT(e)');
        $this->composeQuery($qb, $terms)
              ;
        
        return $qb->getQuery()->getSingleScalarResult();
    }
    
    protected function composeQuery(QueryBuilder &$qb, $terms)
    {
        
        // add security clauses
        $reachableCenters = $this->helper
              ->getReachableCenters($this->user, new Role('CHILL_EVENT_SEE'));
        
        if (count($reachableCenters) === 0) {
            // add a clause to block all events
            $where = $qb->expr()->isNull('e.center');
            $qb->andWhere($where);
        } else {
            
            $n = 0;
            $orWhere = $qb->expr()->orX();
            foreach ($reachableCenters as $center) {
                $circles = $this->helper->getReachableScopes($this->user,
                                  new Role('CHILL_EVENT_SEE'), $center);
                $where = $qb->expr()->andX(
                      $qb->expr()->eq('e.center', ':center_'.$n),
                      $qb->expr()->in('e.circle', ':circle_'.$n)
                      );
                $qb->setParameter('center_'.$n, $center);
                $qb->setParameter('circle_'.$n, $circles);
                $orWhere->add($where);
            }
            
            $qb->andWhere($orWhere);
        }
        
        if (
                (isset($terms['name']) OR isset($terms['_default']))
                AND
                (!empty($terms['name']) OR !empty($terms['_default']))) {
            // the form with name:"xyz" has precedence
            $name = isset($terms['name']) ? $terms['name'] : $terms['_default'];
            
            $where = $qb->expr()->like('UNACCENT(LOWER(e.name))', ':name');
            $qb->setParameter('name', '%'.$name.'%');
            $qb->andWhere($where);
        }
        
        if (isset($terms['date'])) {
            $date = $this->parseDate($terms['date']);
            
            $where = $qb->expr()->eq('e.date', ':date');
            $qb->setParameter('date', $date);
            $qb->andWhere($where);
        }
        
        if (isset($terms['date-from'])) {
            $date = $this->parseDate($terms['date-from']);
            
            $where = $qb->expr()->gte('e.date', ':datefrom');
            $qb->setParameter('datefrom', $date);
            $qb->andWhere($where);
        }
        
        if (isset($terms['date-to'])) {
            $date = $this->parseDate($terms['date-to']);
            
            $where = $qb->expr()->lte('e.date', ':dateto');
            $qb->setParameter('dateto', $date);
            $qb->andWhere($where);
        }
        
        
        
        return $qb;
    }
}
