<?php

namespace Chill\EventBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Chill\PersonBundle\Form\Type\PickPersonType;
use Chill\EventBundle\Entity\Event;
use Chill\EventBundle\Form\EventType;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;


/**
 * Event controller.
 *
 */
class EventController extends Controller
{
    
    public function mostRecentIndexAction()
    {
        return $this->redirectToRoute('chill_main_search', array(
            'q' => '@event'
        ));
    }
    
    /**
     * Creates a new Event entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Event();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans('The event was created'));

            return $this->redirect($this->generateUrl('chill_event__event_show', array('event_id' => $entity->getId())));
        }

        return $this->render('ChillEventBundle:Event:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Event entity.
     *
     * @param Event $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Event $entity)
    {
        $form = $this->createForm(EventType::class, $entity, array(
            'action' => $this->generateUrl('chill_event__event_create'),
            'method' => 'POST'
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Event entity.
     *
     */
    public function newAction()
    {
        $entity = new Event();
        $form   = $this->createCreateForm($entity);

        return $this->render('ChillEventBundle:Event:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Event entity.
     *
     */
    public function showAction($event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillEventBundle:Event')->find($event_id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $this->denyAccessUnlessGranted('CHILL_EVENT_SEE_DETAILS', $entity, 
              "You are not allowed to see details on this event");
        
        $addParticipationByPersonForm = $this->createAddParticipationByPersonForm($entity);

        return $this->render('ChillEventBundle:Event:show.html.twig', array(
            'event'      => $entity,
            'form_add_participation_by_person' => $addParticipationByPersonForm->createView()
        ));
    }
    
    /**
     * create a form to add a participation with a person
     * 
     * @return \Symfony\Component\Form\FormInterface
     */
    protected function createAddParticipationByPersonForm(Event $event)
    {
        /* @var $builder \Symfony\Component\Form\FormBuilderInterface */
        $builder = $this
                ->get('form.factory')
                ->createNamedBuilder( 
                    null,
                    FormType::class, 
                    null,
                    array(
                        'method' => 'GET',
                        'action' => $this->generateUrl('chill_event_participation_new'),
                        'csrf_protection' => false
                    ))
                ;
        
        $builder->add('person_id', PickPersonType::class, array(
            'role' => new Role('CHILL_EVENT_CREATE'),
            'centers' => $event->getCenter()
        ));
        
        $builder->add('event_id', HiddenType::class, array(
            'data' => $event->getId()
        ));
        
        $builder->add('submit', SubmitType::class,
                array(
                    'label' => 'Add a participation'
                ));
        
        return $builder->getForm();
    }

    /**
     * Displays a form to edit an existing Event entity.
     *
     */
    public function editAction($event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillEventBundle:Event')->find($event_id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('ChillEventBundle:Event:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Event entity.
    *
    * @param Event $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Event $entity)
    {
        $form = $this->createForm(EventType::class, $entity, array(
            'action' => $this->generateUrl('chill_event__event_update', array('event_id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->remove('center');

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Event entity.
     *
     */
    public function updateAction(Request $request, $event_id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ChillEventBundle:Event')->find($event_id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Event entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
            
            $this->addFlash('success', $this->get('translator')
                    ->trans('The event was updated'));

            return $this->redirect($this->generateUrl('chill_event__event_edit', array('event_id' => $event_id)));
        }

        return $this->render('ChillEventBundle:Event:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
}
