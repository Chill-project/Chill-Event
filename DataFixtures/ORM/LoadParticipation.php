<?php

namespace Chill\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Chill\EventBundle\Entity\Participation;
use Chill\MainBundle\Entity\Center;
use Chill\EventBundle\Entity\Event;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;

/**
 * Load Events and Participation
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LoadParticipation extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     *
     * @var \Faker\Generator
     */
    protected $faker;
    
    public function __construct()
    {
        $this->faker = \Faker\Factory::create('fr_FR');
    }
    
    public function getOrder()
    {
        return 30010;
    }

    public function load(ObjectManager $manager)
    {
        $centers = $manager->getRepository('ChillMainBundle:Center')
              ->findAll();
        
        foreach($centers as $center) {
            
            $people = $manager->getRepository('ChillPersonBundle:Person')
              ->findBy(array('center' => $center));
            $events = $this->createEvents($center, $manager);

            /* @var $person \Chill\PersonBundle\Entity\Person */
            foreach ($people as $person) {
                $nb = rand(0,3);
                
                for ($i=0; $i<$nb; $i++) {
                    $event = $events[array_rand($events)];
                    $role = $event->getType()->getRoles()->get(
                          array_rand($event->getType()->getRoles()->toArray()));
                    $status = $event->getType()->getStatuses()->get(
                          array_rand($event->getType()->getStatuses()->toArray()));

                    $participation = (new Participation())
                          ->setPerson($person)
                          ->setRole($role)
                          ->setStatus($status)
                          ->setEvent($event)
                          ;
                    $manager->persist($participation);
                }
            }
        }
        
        $manager->flush();
        
        
    }
    
    public function createEvents(Center $center, ObjectManager $manager)
    {
        $expectedNumber = 20;
        $events = array();
        
        for($i=0; $i<$expectedNumber; $i++) {
            $event = (new Event())
                  ->setDate($this->faker->dateTimeBetween('-2 years', '+6 months'))
                  ->setName($this->faker->words(rand(2,4), true))
                  ->setType($this->getReference(LoadEventTypes::$refs[array_rand(LoadEventTypes::$refs)]))
                  ->setCenter($center)
                  ->setCircle(
                        $this->getReference(
                            LoadScopes::$references[array_rand(LoadScopes::$references)]
                              )
                        )
                  ;
            $manager->persist($event);
            $events[] = $event;
        }
        
        return $events;
    }
}
