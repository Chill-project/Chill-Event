<?php

/*
 * Copyright (C) 2016 Julien Fastré <julien.fastre@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


namespace Chill\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Chill\MainBundle\DataFixtures\ORM\LoadPermissionsGroup;
use Chill\MainBundle\Entity\RoleScope;
use Chill\MainBundle\DataFixtures\ORM\LoadScopes;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Add roles to existing groups
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LoadRolesACL extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        foreach (LoadPermissionsGroup::$refs as $permissionsGroupRef) {
            $permissionsGroup = $this->getReference($permissionsGroupRef);
            foreach (LoadScopes::$references as $scopeRef){
                $scope = $this->getReference($scopeRef);
                //create permission group
                switch ($permissionsGroup->getName()) {
                    case 'social':
                        if ($scope->getName()['en'] === 'administrative') {
                            break 2; // we do not want any power on administrative
                        }
                        break;
                    case 'administrative':
                    case 'direction':
                        if (in_array($scope->getName()['en'], array('administrative', 'social'))) {
                            break 2; // we do not want any power on social or administrative
                        }  
                        break;
                }
                
                printf("Adding CHILL_EVENT_UPDATE & CHILL_EVENT_CREATE "
                        . "CHILL_EVENT_PARTICIPATION_UPDATE & CHILL_EVENT_PARTICIPATION_CREATE "
                        . "to %s "
                        . "permission group, scope '%s' \n", 
                        $permissionsGroup->getName(), $scope->getName()['en']);
                $roleScopeUpdate = (new RoleScope())
                            ->setRole('CHILL_EVENT_UPDATE')
                            ->setScope($scope);
                $roleScopeUpdate2 = (new RoleScope())
                            ->setRole('CHILL_EVENT_PARTICIPATION_UPDATE')
                            ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeUpdate);
                $permissionsGroup->addRoleScope($roleScopeUpdate2);
                $roleScopeCreate = (new RoleScope())
                            ->setRole('CHILL_EVENT_CREATE')
                            ->setScope($scope);
                $roleScopeCreate2 = (new RoleScope())
                            ->setRole('CHILL_EVENT_PARTICIPATION_CREATE')
                            ->setScope($scope);
                $permissionsGroup->addRoleScope($roleScopeCreate);
                $permissionsGroup->addRoleScope($roleScopeCreate2);
                $manager->persist($roleScopeUpdate);
                $manager->persist($roleScopeUpdate2);
                $manager->persist($roleScopeCreate);
                $manager->persist($roleScopeCreate2);
            }
            
        }
        
        $manager->flush();
    }

    public function getOrder()
    {
        return 30011;
    }

}
