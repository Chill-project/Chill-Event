<?php


namespace Chill\EventBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Chill\EventBundle\Entity\EventType;
use Chill\EventBundle\Entity\Role;
use Chill\EventBundle\Entity\Status;

/**
 * Load a set of `EventType`, their `Role` and `Status`.
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 * @author Champs Libres <info@champs-libres.coop>
 */
class LoadEventTypes extends AbstractFixture implements OrderedFixtureInterface
{
    public static $refs = array();
    
    public function getOrder()
    {
        return 30000;
    }

    public function load(ObjectManager $manager)
    {
        $type = (new EventType())
              ->setActive(true)
              ->setName(array('fr' => 'Échange de savoirs', 'en' => 'Exchange of knowledge'))
              ;
        $manager->persist($type);
        $this->addReference('event_type_knowledge', $type);
        self::$refs[] = 'event_type_knowledge';
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'))
                ->setType($type)
              ;
        $manager->persist($role);
        
        $role = (new Role())
              ->setActive(true)
              ->setName(array('fr' => 'Animateur'))
              ->setType($type);
        $manager->persist($role);
        
        $status = (new Status())
              ->setActive(true)
              ->setName(array('fr' => 'Inscrit'))
              ->setType($type);
        $manager->persist($status);
        
        $status = (new Status())
              ->setActive(true)
              ->setName(array('fr' => 'Présent'))
              ->setType($type)
              ;
        $manager->persist($status);
        
        
        $type = (new EventType())
              ->setActive(true)
              ->setName(array('fr' => 'Formation', 'en' => 'Course', 'nl' => 'Opleiding'))
              ;
        $manager->persist($type);
        $this->addReference('event_type_course', $type);
        self::$refs[] = 'event_type_course';
        
        $role = (new Role())
                ->setActive(true)
                ->setName(array('fr' => 'Participant', 'nl' => 'Deelneemer', 'en' => 'Participant'))
                ->setType($type)
              ;
        $manager->persist($role);
        
        $status = (new Status())
              ->setActive(true)
              ->setName(array('fr' => 'Inscrit'))
              ->setType($type)
              ;
        $manager->persist($status);
        
        $status = (new Status())
              ->setActive(true)
              ->setName(array('fr' => 'En liste d\'attente'))
              ->setType($type)
              ;
        $manager->persist($status);
        
        $manager->flush();
    }

}
