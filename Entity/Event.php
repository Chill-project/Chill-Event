<?php

namespace Chill\EventBundle\Entity;

use Chill\MainBundle\Entity\HasCenterInterface;
use Chill\MainBundle\Entity\HasScopeInterface;

/**
 * Event
 */
class Event implements HasCenterInterface, HasScopeInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var \DateTime
     */
    private $date;
    
    /**
     *
     * @var \Chill\MainBundle\Entity\Center
     */
    private $center;
    
    /**
     *
     * @var EventType
     */
    private $type;
    
    /**
     *
     * @var \Chill\MainBundle\Entity\Scope
     */
    private $circle;
    
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $participations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->participations = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return Event
     */
    public function setName($label)
    {
        $this->name = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Event
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    
    public function setCenter(\Chill\MainBundle\Entity\Center $center)
    {
        $this->center = $center;
        
        return $this;
    }
    
    /**
     *
     * @return EventType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     *
     * @param \Chill\EventBundle\Entity\EventType $type
     * @return \Chill\EventBundle\Entity\Event
     */
    public function setType(EventType $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     *
     * @return \Chill\MainBundle\Entity\Center
     */
    public function getCenter()
    {
        return $this->center;
    }
    
    /**
     *
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getCircle()
    {
        return $this->circle;
    }

    /**
     *
     * @param \Chill\MainBundle\Entity\Scope $circle
     * @return \Chill\EventBundle\Entity\Event
     */
    public function setCircle(\Chill\MainBundle\Entity\Scope $circle)
    {
        $this->circle = $circle;
        return $this;
    }
    
    /**
     *
     * @deprecated
     * @return \Chill\MainBundle\Entity\Scope
     */
    public function getScope()
    {
        return $this->getCircle();
    }

    
    /**
     * Add participation
     *
     * @param \Chill\EventBundle\Entity\Participation $participation
     *
     * @return Event
     */
    public function addParticipation(\Chill\EventBundle\Entity\Participation $participation)
    {
        $this->participations[] = $participation;

        return $this;
    }

    /**
     * Remove participation
     *
     * @param \Chill\EventBundle\Entity\Participation $participation
     */
    public function removeParticipation(\Chill\EventBundle\Entity\Participation $participation)
    {
        $this->participations->removeElement($participation);
    }

    /**
     * Get participations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticipations()
    {
        return $this->participations;
    }
}
