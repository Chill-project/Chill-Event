<?php

namespace Chill\EventBundle\Entity;

/**
 * Status
 */
class Status
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $name;

    /**
     * @var boolean
     */
    private $active;
    
    /**
     * @var \Chill\EventBundle\Entity\EventType
     */
    private $type;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param array $name
     *
     * @return Status
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get label
     *
     * @return array
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Status
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }


    /**
     * Set type
     *
     * @param \Chill\EventBundle\Entity\EventType $type
     *
     * @return Status
     */
    public function setType(\Chill\EventBundle\Entity\EventType $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Chill\EventBundle\Entity\EventType
     */
    public function getType()
    {
        return $this->type;
    }
}
