<?php

namespace Chill\EventBundle\Entity;

use Chill\MainBundle\Entity\HasScopeInterface;
use Chill\MainBundle\Entity\HasCenterInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Participation
 */
class Participation implements HasCenterInterface, HasScopeInterface, \ArrayAccess
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $lastUpdate;
    
    /**
     * @var \Chill\EventBundle\Entity\Event
     */
    private $event;

    /**
     * @var \Chill\PersonBundle\Entity\Person
     */
    private $person;

    /**
     * @var \Chill\EventBundle\Entity\Role
     */
    private $role;

    /**
     * @var \Chill\EventBundle\Entity\Status
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return Participation
     */
    protected function update()
    {
        $this->lastUpdate = new \DateTime('now');

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }


    /**
     * Set event
     *
     * @param \Chill\EventBundle\Entity\Event $event
     *
     * @return Participation
     */
    public function setEvent(\Chill\EventBundle\Entity\Event $event = null)
    {
        if ($this->event !== $event) {
            $this->update();
        }
        
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \Chill\EventBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set person
     *
     * @param \Chill\PersonBundle\Entity\Person $person
     *
     * @return Participation
     */
    public function setPerson(\Chill\PersonBundle\Entity\Person $person = null)
    {
        if ($person !== $this->person) {
            $this->update();
        }
        
        $this->person = $person;

        return $this;
    }

    /**
     * Get person
     *
     * @return \Chill\PersonBundle\Entity\Person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Set role
     *
     * @param \Chill\EventBundle\Entity\Role $role
     *
     * @return Participation
     */
    public function setRole(\Chill\EventBundle\Entity\Role $role = null)
    {
        if ($role !== $this->role) {
            $this->update();
        }
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \Chill\EventBundle\Entity\Role
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set status
     *
     * @param \Chill\EventBundle\Entity\Status $status
     *
     * @return Participation
     */
    public function setStatus(\Chill\EventBundle\Entity\Status $status = null)
    {
        if ($this->status !== $status) {
            $this->update();
        }
        
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \Chill\EventBundle\Entity\Status
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getCenter()
    {
        if ($this->getEvent() === NULL) {
            throw new \RuntimeException('The event is not linked with this instance. '
                    . 'You should initialize the event with a valid center before.');
        }
        
        return $this->getEvent()->getCenter();
    }

    public function getScope()
    {
        if ($this->getEvent() === NULL) {
            throw new \RuntimeException('The event is not linked with this instance. '
                    . 'You should initialize the event with a valid center before.');
        }
        
        return $this->getEvent()->getCircle();
    }
    
    /**
     * Check that : 
     * 
     * - the role can be associated with this event type
     * - the status can be associated with this event type
     * 
     * @param ExecutionContextInterface $context
     */
    public function isConsistent(ExecutionContextInterface $context)
    {
        
        if ($this->getEvent() === NULL || $this->getRole() === NULL || $this->getStatus() === NULL) {
            return;
        }
        
        if ($this->getRole()->getType()->getId() !== 
                $this->getEvent()->getType()->getId()) {
            $context->buildViolation('The role is not allowed with this event type')
                    ->atPath('role')
                    ->addViolation();
        }
        
        if ($this->getStatus()->getType()->getId() !==
                $this->getEvent()->getType()->getId()) {
            $context->buildViolation('The status is not allowed with this event type')
                    ->atPath('status')
                    ->addViolation();
        }
    }

    public function offsetExists($offset)
    {
        return in_array($offset, array(
            'person', 'role', 'status', 'event'
        ));
    }

    public function offsetGet($offset)
    {
        switch ($offset) {
            case 'person':
                return $this->getPerson();
                break;
            case 'role':
                return $this->getRole();
                break;
            case 'status':
                return $this->getStatus();
                break;
            case 'event':
                return $this->getEvent();
                break;
        }
    }

    public function offsetSet($offset, $value)
    {
        switch($offset) {
            case 'person':
                return $this->setPerson($value);
                break;
            case 'role':
                return $this->setRole($value);
                break;
            case 'status':
                return $this->setStatus($value);
                break;
            case 'event':
                return $this->setEvent($value);
                break;
        }
    }

    public function offsetUnset($offset)
    {
        $this->offsetSet($offset, null);
    }

}
