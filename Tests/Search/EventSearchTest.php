<?php

/*
 * Copyright (C) 2016 Champs-Libres <info@champs-libres.coop>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Chill\EventBundle\Tests\Search;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Chill\EventBundle\Entity\Event;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Chill\EventBundle\Search\EventSearch;


/**
 * Test the EventSearch class
 *
 * @author Julien Fastré <julien.fastre@champs-libres.coop>
 */
class EventSearchTest extends WebTestCase
{
    /**
     * The eventSearch service, which is used to search events
     *
     * @var \Chill\EventBundle\Search\EventSearch
     */
    protected $eventSearch;
    
    /**
     *
     * @var \Doctrine\ORM\EntityManagerInterface
     */
    protected $entityManager;
    
    /**
     * The center A
     * 
     * @var \Chill\MainBundle\Entity\Center
     */
    protected $centerA;
    
    /**
     * a random event type
     *
     * @var \Chill\EventBundle\Entity\EventType
     */
    protected $eventType;
    
    /**
     * Events created during this test
     *
     * @var Event[]
     */
    protected $events = array();
    
    /**
     *
     * @var \Prophecy\Prophet
     */
    protected $prophet;
    
    /**
     *
     * @var \Symfony\Component\BrowserKit\Client
     */
    protected $client;
    
    public function setUp()
    {
        self::bootKernel();
        /* @var $kernel \Symfony\Component\HttpKernel\KernelInterface */
        $kernel = self::$kernel;
        
        $this->client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'center a_social',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
        
        $this->prophet = new \Prophecy\Prophet;
        
        $this->entityManager = self::$kernel->getContainer()
                ->get('doctrine.orm.entity_manager')
                ;
        
        $this->centerA = $this->entityManager
                ->getRepository('ChillMainBundle:Center')
                ->findOneBy(array('name' => 'Center A'));
        
        $this->eventType = $this->entityManager
                ->getRepository('ChillEventBundle:EventType')
                ->findAll()[0];
        
        $this->createEvents();
    }
    
    public function tearDown()
    {
        foreach ($this->events as $event) {
            $this->entityManager->createQuery('DELETE FROM ChillEventBundle:Event e WHERE e.id = :event_id')
                    ->setParameter('event_id', $event->getId())
                    ->execute();
        }
        
        $this->events = array();
    }
    
    protected function createEvents()
    {
        $event1 = (new Event())
                ->setCenter($this->centerA)
                ->setDate(new \DateTime('2016-05-30'))
                ->setName('Printemps européen')
                ->setType($this->eventType)
                ->setCircle($this->getCircle())
                ;
        $this->entityManager->persist($event1);
        $this->events[] = $event1;
        
        $event2 = (new Event())
                ->setCenter($this->centerA)
                ->setDate(new \DateTime('2016-06-24'))
                ->setName('Hiver de la droite')
                ->setType($this->eventType)
                ->setCircle($this->getCircle())
                ;
        $this->entityManager->persist($event2);
        $this->events[] = $event2;
        
        $this->entityManager->flush();
    }
    
    /**
     * 
     * @param string $name the name of the circle
     * @return \Chill\MainBundle\Entity\Scope
     */
    protected function getCircle($name = 'social')
    {
        $circles = $this->entityManager->getRepository('ChillMainBundle:Scope')
                ->findAll();
        
        /* @var $circle \Chill\MainBundle\Entity\Scope */
        foreach($circles as $circle) {
            if (in_array($name, $circle->getName())) {
                return $circle;
            }
        }
    }
    
    public function testDisplayAll()
    {
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events'
            ));
        
        $this->assertGreaterThanOrEqual(2, $crawler->filter('table.events tr')->count(),
                'assert than more than 2 tr are present');
    }
    
    public function testSearchByDefault()
    {
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events printemps'
            ));

        $this->assertEquals(
                1, 
                $crawler->filter('table.events tr')->count() - 1 /* as the header is a th */,
                'assert than more than 2 tr are present');
        
        $this->assertEquals(
                1, 
                $crawler->filter('tr:contains("Printemps")')->count(),
                'assert that the word "printemps" is present');
    }
    
    public function testSearchByName()
    {
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events name:printemps'
            ));

        $this->assertEquals(
                1, 
                $crawler->filter('table.events tr')->count() - 1 /* as the header is a th */,
                'assert than more than 2 tr are present');
        
        $this->assertEquals(
                1, 
                $crawler->filter('tr:contains("Printemps")')->count(),
                'assert that the word "printemps" is present');
    }
    
    public function testSearchByDateDateFromOnly()
    {
        // search with date from
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events date-from:2016-05-30'
            ));
        /* @var $dateFrom \DateTime the date from in DateTime */
        $dateFrom = \DateTime::createFromFormat("Y-m-d", "2016-05-30");
        
        $dates = $this->iterateOnRowsToFindDate($crawler->filter("tr"));
        
        foreach($dates as $date) {
            $this->assertGreaterThanOrEqual($dateFrom, $date);
        }
        
        // click on link "Voir tous les résultats"
        $crawlerAllResults = $this->client->click($crawler
                ->selectLink("Voir tous les résultats")->link());
        $dates = $this->iterateOnRowsToFindDate($crawlerAllResults->filter("tr"));
        
        foreach ($dates as $date) {
            $this->assertGreaterThanOrEqual($dateFrom, $date);
        }
        
        //iterate on pagination
        $crawlerAllResults->filter(".pagination a")->each(function($a, $i) use ($dateFrom) {
            $page = $this->client->click($a->link());
            $dates = $this->iterateOnRowsToFindDate($page->filter("tr"));
            
            foreach($dates as $date) {
                $this->assertGreaterThanOrEqual($dateFrom, $date);
            }
        });
    }
        
    public function testSearchByDateDateBetween()
    {
        // serach with date from **and** date-to
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events date-from:2016-05-30 date-to:2016-06-20'
            ));
        
        /* @var $dateFrom \DateTime the date from in DateTime */
        $dateFrom = \DateTime::createFromFormat("Y-m-d", "2016-05-30");
        $dateTo   = \DateTime::createFromFormat("Y-m-d", "2016-06-20");
        
        $dates = $this->iterateOnRowsToFindDate($crawler->filter("tr"));
        
        foreach($dates as $date) {
            $this->assertGreaterThanOrEqual($dateFrom, $date);
            $this->assertLessThanOrEqual($dateTo, $date);
        }
        
        // there should not have any other results, but if any other bundle 
        // add some other event, we go on next pages
        
        if ($crawler->selectLink("Voir tous les résultats")->count() == 0) {
            return ;
        }
        
        // click on link "Voir tous les résultats"
        $crawlerAllResults = $this->client->click($crawler
                ->selectLink("Voir tous les résultats")->link());
        $dates = $this->iterateOnRowsToFindDate($crawlerAllResults->filter("tr"));
        
        foreach ($dates as $date) {
            $this->assertGreaterThanOrEqual($dateFrom, $date);
            $this->assertLessThanOrEqual($dateTo, $date);
        }
        
        //iterate on pagination
        $crawlerAllResults->filter(".pagination a")->each(function($a, $i) use ($dateFrom) {
            $page = $this->client->click($a->link());
            $dates = $this->iterateOnRowsToFindDate($page->filter("tr"));
            
            foreach($dates as $date) {
                $this->assertGreaterThanOrEqual($dateFrom, $date);
                $this->assertLessThanOrEqual($dateTo, $date);
            }
        });
    }
    
    public function testSearchByDateDateTo()
    {
        
        // serach with date from **and** date-to
        $crawler = $this->client->request('GET', '/fr/search', array(
                'q' => '@events date:2016-05-30'
            ));

        /* @var $dateFrom \DateTime the date from in DateTime */
        $dateTo   = \DateTime::createFromFormat("Y-m-d", "2016-05-30");
        
        $dates = $this->iterateOnRowsToFindDate($crawler->filter("tr"));
        
        foreach($dates as $date) {
            $this->assertLessThanOrEqual($dateTo, $date);
        }
        
        if ($crawler->selectLink("Voir tous les résultats")->count() == 0) {
            return ;
        }
        
        // click on link "Voir tous les résultats"
        $crawlerAllResults = $this->client->click($crawler
                ->selectLink("Voir tous les résultats")->link());
        $dates = $this->iterateOnRowsToFindDate($crawlerAllResults->filter("tr"));
        
        foreach ($dates as $date) {
            $this->assertLessThanOrEqual($dateTo, $date);
        }
        
        //iterate on pagination
        $crawlerAllResults->filter(".pagination a")->each(function($a, $i) use ($dateFrom) {
            $page = $this->client->click($a->link());
            $dates = $this->iterateOnRowsToFindDate($page->filter("tr"));
            
            foreach($dates as $date) {
                $this->assertLessThanOrEqual($dateTo, $date);
            }
        });
        
    }
    
    /**
     * this function iterate on row from results of events and return the content
     * of the second column (which should contains the date) in DateTime objects
     * 
     * @param \Symfony\Component\DomCrawler\Crawler $trs
     * @return \DateTime[]
     */
    private function iterateOnRowsToFindDate(\Symfony\Component\DomCrawler\Crawler $trs)
    {
        $months = array(
            "janvier" => 1,
            "février" => 2,
            "mars"    => 3,
            "avril"   => 4,
            "mai"     => 5,
            "juin"    => 6,
            "juillet" => 7,
            "août"    => 8,
            "septembre" => 9,
            "octobre"   => 10,
            "novembre"  => 11,
            "décembre"  => 12
        );
        
        
        $results = $trs->each(function($tr, $i) use ($months) {
            // we skip the first row
            if ($i > 0) {
                // get the second node, which should contains a date
                $tdDate = $tr->filter("td")->eq(1);
                // transform the date, which should be in french, into a DateTime object
                $parts = explode(" ", $tdDate->text());
                return \DateTime::createFromFormat("Y-m-d", $parts[2].
                        "-".$months[$parts[1]]."-".$parts[0]);
            }
        });
        
        // remove the first row
        unset($results[0]);
        
        return $results;
    }
    
    /**
     * Test that a user connected with an user with the wrong center does not
     * see the events
     */
    public function testDisplayAllWrongUser()
    {
        $client = static::createClient(array(), array(
           'PHP_AUTH_USER' => 'center b_social',
           'PHP_AUTH_PW'   => 'password',
           'HTTP_ACCEPT_LANGUAGE' => 'fr_FR'
        ));
        
        $crawler = $client->request('GET', '/fr/search', array(
                'q' => '@events printemps'
            ));
        
        $this->assertEquals(0, $crawler->filter('tr:contains("Printemps")')->count(),
                'assert that the word "printemps" is present');
        
    }
    
    
    
}
